import pandas as pd
import numpy as np
import os
import re
import argparse

code_FC = "FBVDA"       # Fiction Bande dessiné Vidéo Documentaire Audio
code_FM = "RBVECMGNO"    # Relié Broché dVd format Européen Comic Manga cD roman
                         # Graphique Numérique O comme grand format
code_G = "AZERTYUIOPQSDFGHJKLMWXCVBN"

#################################################################### Fonctions :
#################################################################### Menus :

def menu():
    """
    Lauch the main menu of the Melampitta library manager
    """
    choice = input("Que souhaitez vous faire ?\n\n1. Rajouter un nouveau livre au catalogue\n2. Rechercher un document\n3. Quitter\n\n-> ")
    if (choice == "1"):
        menu_new_entry()
    elif (choice == "2"):
        menu_research_entry()
    elif (choice == "3"):
        print("\n\n ~ Au revoir ~")
    else:
        print("\n\n Erreur Valeur non conforme\n\n")
        menu()

def menu_new_entry():
    """
    Launch the 'New Entry' menu
    """
    while(input("Voulez vous rajouter un bouquin ? (y/N) -> ") == "y"):
        isbn = str(input("Veuillez rentrer l'isbn' du bouquin (si aucun isbn, mettez de None) -> "))
        code = check_if_code("Veuillez rentrer le code du bouquin -> ")
        name = str(input("Veuillez rentrer le nom du bouquin -> "))
        author = str(input("Veuillez rentrer le nom de ou des auteur/autrice (si plusieurs entrées, veuillez mettre un '|' entre chaque entrée) -> "))
        date = check_int("Veuillez rentrer la première date de parution du bouquin -> ")
        page= check_int("Veuillez rentrer le nombre de pages du bouquin -> ")
        language = str(input("Veuillez rentrer la ou les langues du bouquin (si plusieurs entrées, veuillez mettre un '|' entre chaque entrée) -> "))
        catal.loc[len(catal)] = [isbn, code, name, author, date, page, language, 0, 0, "Bourg-en-Bresse", "None", 0]
        display_entry(len(catal) - 1)
    menu()

def menu_research_entry():
    """
    Launch the 'Research' menu
    """
    search_by_index()
    menu()


#################################################################### Display :

def display_entry(index):
    """
    Display a given entry of the catalog, given by its index
    """
    print("\n\n                     -----     Entrée n° ", index,", Fiche bibliographique :     -----\n")
    print("                               ISBN :           ", catal["ISBN"].loc[index])
    print("                               Code :           ", catal["Code"].loc[index])
    print("                               Nom :            ", catal["Name"].loc[index])
    print("                               Auteur(s) :      ", display_list(catal["Author"].loc[index]))
    print("                               Parution :       ", catal["Date"].loc[index])
    print("                               Pages :          ", catal["Page"].loc[index])
    print("                               Langue(s) :      ", display_list(catal["Language"].loc[index]))
    print("                               Difficulté :     ", catal["Opinion"].loc[index],"/5")
    print("                               Localisation :   ", catal["Loan"].loc[index])
    print("                               Thèmes :         ", catal["Theme"].loc[index])

def display_list(str):
    """
    Lit une entrée catalogue et la retourne lisible sous forme de chaîne de
    charactères
    """
    ret = ""

    for letter in str:
        if (letter == "|"):
            ret = ret + ", "
        else:
            ret = ret + letter

    return ret

####################################################################    Search :

def search_by_index():
    """
    Ask for an index and display the entry associated with it
    """
    index = int(input("\n\nRecherche par Index : -> "))
    display_entry(index)

####################################################################    Search :

def check_if_code(input_str):
    """
    Check if a string can be a code entry
    """
    """
    code_FC = "FBVDA"       # Fiction Bande dessiné Vidéo Documentaire Audio
    code_FM = "RBVECMGN"    # Relié Broché dVd format Européen Comic Manga cD roman Graphique Numérique
    code_G = "AZERTYUIOPQSDFGHJKLMWXCVBN"
    """

    while True:
        var = input(input_str)
        if re.match(f"[{code_FC}][{code_FM}][{code_G}][0-9]{{1,3}}", var):
            return var
        else:
            print("Erreur Valeur non conforme")


def check_if_language(input_str):
    """
    Check if a string can be a language entry
    """


def check_int(input_str):
    while True:
        try:
            var = int(input(input_str))
        except ValueError:
            print("Erreur Valeur non conforme")
            continue
        return var



################################################################################
####################################################################      Main :

# Retrieve entries

parser = argparse.ArgumentParser()
parser.add_argument("input", help="input file name")
args = parser.parse_args()


# Opening the catalog
os.system("clear")
catal = pd.read_csv(args.input, sep=",", index_col="Num",verbose = True)
catal["Opinion"] = catal["OpinionC"]/catal["OpinionNb"]

# Main
print("\n                  -----     Catalogue de la bibliothèque Malampitta :     -----\n\n")
print(catal[["ISBN", "Code", "Name", "Author", "Opinion"]])
print("\n                              -----     Catalogue chargé     -----\n\n")

menu()



#zone de Test
#check_if_code("test")

#Cleaning and saving the catalog
catal = catal.drop("Opinion",axis=1)
catal = catal.reindex(np.arange(len(catal["ISBN"]), dtype=int))
print("\n\n", catal[["ISBN", "Code", "Name", "Author"]])
catal.to_csv(args.input, sep=",")
